import json
import logging
import os
import threading


from gi.repository import GLib

from installer.douane.command_executor import CommandExecutor
from installer.event_broadcaster_states import EventBroadcasterStates

class DouaneInstallerListener:
    """
    This class listens to the named pipe and triggers the given
    `on_failure` and `on_step_update` methods based on the received
    data.

    This is done within a thread which closes automatically when this installer
    closes, thanks to the `daemon=True` passed to the `threading.Thread`
    method.
    """
    def __init__(self, configuration, on_failure, on_success, on_step_update):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.configuration = configuration

        # UI Events to be triggered
        self.install_failed = on_failure
        self.install_suceeded = on_success
        self.update_state = on_step_update

    def start(self):
        """
        Creates a named pipe and listen to this pipe from a thread
        """
        self.logger.debug(f'Creating listen thread ...')
        threading.Thread(target=self.__listen_thread, daemon=True).start()

    def __listen_thread(self):
        """
        Listen to the named pipe and triggers some events based on what has been
        received from the installer.
        """
        self.logger.debug(f'Running the listen thread ...')

        self.logger.debug('Listening the pipe from '
                          f'{self.configuration.douane_pipe_path} ...')

        with open(self.configuration.douane_pipe_path) as pipe:
            self.logger.debug('Pipe is opened')
            while True:
                data = pipe.readline()

                if len(data) == 0:
                    self.logger.debug('Closing the pipe!')
                    pipe.close()
                    self.logger.debug('Breaking!')
                    break

                self.logger.debug(f'Received data: {data}')

                command, args = self.__parse(data)

                if self.__process_command(command, args):
                    break

        self.logger.debug('DouaneInstallerListener LEAVED !')
        return True

    def __parse(self, data):
        command, dict = data.split('|')

        dict = dict.replace("'", '"')
        args = None

        try:
            args = json.loads(dict)
            self.logger.debug(f'command: {command}, args: {args}')
        except json.decoder.JSONDecodeError:
            self.logger.debug(f'ERROR decoding {dict}')
            pass

        return command, args

    def __process_command(self, command, args = None):
        if command == EventBroadcasterStates.FAILURE:
            self.logger.debug('Command failure received with '
                              f'args {args}')
            if 'error' in args:
                GLib.idle_add(self.install_failed, args['error'])
            else:
                GLib.idle_add(self.install_failed)

            return True
        elif command == EventBroadcasterStates.SUCCESS:
            self.logger.debug('Installation success received')
            GLib.idle_add(self.install_suceeded)
            return True
        elif command == EventBroadcasterStates.AUTOSTART_DOUANE_DIALOG:
            self.logger.debug('Autostart douane-dialog request received')
            self.__autostart_douane_dialog()
            return False
        elif command == EventBroadcasterStates.UPDATE_STATE:
            self.logger.debug('Update state command received')
            GLib.idle_add(self.update_state, args['step'], args['state'])
            return False

    def __autostart_douane_dialog(self):
        if self.__install_dialog_autostart():
            self.__start_detached_douane_dialog()

    def __install_dialog_autostart(self):
        command = ['douane-configurator', '--install-autostart']
        completed_process = CommandExecutor().execute(command)

        if not completed_process.returncode == 0:
            self.logger.debug(f'Command {command.join(" ")} execution failed: '
                              f'{completed_process.stderr.decode("utf-8")}')
            return False

        return True

    def __start_detached_douane_dialog(self):
        command = ['/opt/douane/bin/douane-dialog']
        completed_process = CommandExecutor().spwan(command)
        return True
