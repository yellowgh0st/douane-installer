import os.path

def __version_file_path():
    init_path = os.path.abspath(os.path.dirname(__file__))
    return os.path.join(init_path, '../../VERSION')

with open(__version_file_path(), 'r') as file:
    VERSION = file.read().replace('\n', '')
