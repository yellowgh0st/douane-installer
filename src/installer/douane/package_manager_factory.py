import logging


from douane.package_managers import AptPackageManager
from douane.package_managers import DnfPackageManager

class PackageManagerFactory:
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

        self._creators = {}

        self.__register_all_formats()

    def register_format(self, format, creator):
        self.logger.debug(f'Registering format {format}')
        self._creators[format] = creator

    def create(self, format):
        creator = self._creators.get(format)

        if not creator:
            raise ValueError(format)

        self.logger.debug(f'Returning creator for format {format}')

        return creator()

    def __register_all_formats(self):
        self.register_format('APT', AptPackageManager)
        self.register_format('RPM', DnfPackageManager)
