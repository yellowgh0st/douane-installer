import logging
import subprocess


class CommandExecutor:
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

    def execute(self, command, cwd=None):
        self.logger.debug(f'running command {" ".join(command)}'
                          f' from {cwd if cwd else "current directory"} ...')

        completed_process = subprocess.run(
            command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            cwd=cwd
        )

        self.logger.debug(f'Command exit code: {completed_process.returncode}')

        if completed_process.returncode == 0:
            self.logger.debug('Command succeeded')
            return completed_process
        else:
            self.logger.debug(f'Command failed: {completed_process.stderr} ...')
            return completed_process

    def spwan(self, command):
        self.logger.debug(f'spwaning command {" ".join(command)} ...')

        subprocess.Popen(command)
