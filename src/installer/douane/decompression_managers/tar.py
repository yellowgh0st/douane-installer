import logging
import re


from douane.command_executor import CommandExecutor

class TarDecompressionManager:
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.destination = None
        self.folder_path = None

    def decompress(self, archive_path):
        self.destination = re.sub(rf'\.tar.gz$', '', archive_path)

        self.logger.debug(f'decompressing {archive_path} to {self.destination} '
                          '...')

        self.__build_and_run_mkdir_command()
        return self.__build_and_run_decompression_command(archive_path)

    def on_failure(self, callback):
        self.__on_failure_callback = callback

    def __build_decompression_command(self, archive_path):
        return [
            'tar', 'xf', archive_path, '--directory', self.destination,
            '--strip', '1'
        ]

    def __build_mkdir_command(self):
        return ['mkdir', '-p', self.destination]

    def __build_and_run_decompression_command(self, archive_path):
        command = self.__build_decompression_command(archive_path)
        completed_process = CommandExecutor().execute(command)

        if completed_process.returncode != 0:
            self.__on_failure_callback(
                f'Error while unzipping the {archive_path} '
                f'file: {completed_process.stderr.decode("utf-8")}')
            return False
        else:
            self.folder_path = self.destination
            return True

    def __build_and_run_mkdir_command(self):
        command = self.__build_mkdir_command()
        completed_process = CommandExecutor().execute(command)

        if completed_process.returncode != 0:
            self.__on_failure_callback(
                f'Error while creating distination folder {self.destination}: '
                f'{completed_process.stderr.decode("utf-8")}')

        return completed_process.returncode == 0
