import logging


from douane.decompression_managers import TarDecompressionManager

class DecompressionManagerFactory:
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

        self._creators = {}

        self.__register_all_formats()

    def register_decompression_tool(self, name, creator):
        self.logger.debug(f'Registering decompression tool {name}')
        self._creators[name] = creator

    def create(self, name):
        creator = self._creators.get(name)

        if not creator:
            raise ValueError(name)

        self.logger.debug(f'Returning creator for name {name}')
        return creator()

    def __register_all_formats(self):
        self.register_decompression_tool('tar', TarDecompressionManager)
        # self.register_decompression_tool('unzip', UnzipDecompressionManager)
