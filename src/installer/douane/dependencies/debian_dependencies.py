import logging


class DebianDependencies:
    """
    Douane dependencies for the Debian Linux distribution.
    """
    def __init__(self, configuration):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.configuration = configuration

    def get_compilation_packages(self):
        return self.__compilation_packages()

    def get_package_list(self):
        return self.__compilation_packages() + \
               self.__decompression_manager_packages() + \
               self.__download_manager_packages() + \
               self.__runtime_packages()

    def __compilation_packages(self):
        return [
            'build-essential',
            'libboost-filesystem-dev',
            'libboost-regex-dev',
            'libboost-signals-dev',
            'libdbus-1-dev',
            'libdbus-c++-dev',
            'libgtkmm-3.0-dev',
            'liblog4cxx-dev',
            'libssl-dev'
        ]

    def __decompression_manager_packages(self):
        """
        Adds `tar` to the dependencies to be installed in the case
        no decompression manager/tool has been found by the Environment discover
        method.
        """
        packages = []

        if self.configuration.download_manager is None:
            packages.append('tar')

        return packages

    def __download_manager_packages(self):
        """
        Adds `wget` to the dependencies to be installed in the case
        no download manager has been found by the Environment discover
        method.
        """
        packages = []

        if self.configuration.download_manager is None:
            packages.append('wget')

        return packages

    def __runtime_packages(self):
        return [
            'dkms',
            'gtk+3.0',
            'policykit-1',
            'python3',
            'python3-dbus',
            'python3-gi'
        ]
