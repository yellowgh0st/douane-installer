import logging


from douane.dependencies_manager_factory import DependenciesManagerFactory
from douane.package_manager_factory import PackageManagerFactory

class SystemCleaner():
    def __init__(self, configuration, on_failure):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.configuration = configuration
        self.on_failure = on_failure

    def start(self):
        self.logger.debug('start ...')

        package_manager = self.__initialise_package_manager()
        package_manager.on_failure(self.on_failure)

        dependencies_manager = self.__initialise_dependencies_manager()
        packages = dependencies_manager.get_compilation_packages()
        packages.sort()

        return package_manager.uninstall(packages)

    def __initialise_dependencies_manager(self):
        factory = DependenciesManagerFactory()

        return factory.create(self.configuration.distribution[0],
                              self.configuration)

    def __initialise_package_manager(self):
        factory = PackageManagerFactory()

        return factory.create(self.configuration.package_format)
