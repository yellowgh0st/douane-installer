import logging


from douane.download_managers.wget import WgetDownloadManager

class DownloadManagerFactory:
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

        self._creators = {}

        self.__register_all_formats()

    def register_downloader(self, name, creator):
        self.logger.debug(f'Registering downloader {name}')
        self._creators[name] = creator

    def create(self, name):
        creator = self._creators.get(name)

        if not creator:
            raise ValueError(name)

        self.logger.debug(f'Returning creator for name {name}')
        return creator()

    def __register_all_formats(self):
        self.register_downloader('wget', WgetDownloadManager)
        # self.register_downloader('curl', CurlDownloadManager)
