import logging
import os


from douane.command_executor import CommandExecutor

class Symlinker():
    """
    Replaces the `dbus/interface` folder from the given repository by
    a symbolink link to the douane dbus sources folder.
    """
    def __init__(self, working_directory, options={}):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.options = options
        self.sources_path = None
        self.working_directory = working_directory

    def name(self):
        return 'Symlinker'

    def compile(self, sources_path):
        self.logger.debug(f'Symlinking {sources_path} to another places ...')

        self.sources_path = sources_path

        if isinstance(self.options["link_in_to"], list):
            for module in self.options["link_in_to"]:
                if not self.__symlink_sources_path_to(module):
                    return False # breaks the loop

            return True
        else:
            self.logger.debug('No given link_in_to, skipping ...')
            return True

    def uninstall(self, sources_path):
        """
        Nothing to do here as the symlinked folders are required in order to
        run repositories uninstallers.
        """
        return True

    def __build_interface_folder_path_for(self, module):
        module_path = self.__search_module_path(module)
        self.logger.debug(f'Module path is {module_path}')
        return os.path.join(module_path, 'src', 'dbus', 'interface')

    def __build_symlink_command_for(self, module):
        interface_folder_path = self.__build_interface_folder_path_for(module)

        return ["ln", "-s", self.sources_path + '/', interface_folder_path]

    def __build_rm_rf_interface_command_for(self, module):
        interface_folder_path = self.__build_interface_folder_path_for(module)

        return ["rm", "-rf", interface_folder_path]

    def __search_module_path(self, module):
        for fname in os.listdir(self.working_directory):
            path = os.path.join(self.working_directory, fname)
            if os.path.isdir(path):
                if module in fname:
                    return path

    def __lns_s_as_interface_folder_for(self, module):
        command = self.__build_symlink_command_for(module)
        completed_process = CommandExecutor().execute(command)

        return completed_process.returncode == 0

    def __rm_fr_interface_folder_for(self, module):
        command = self.__build_rm_rf_interface_command_for(module)
        completed_process = CommandExecutor().execute(command)

        return completed_process.returncode == 0

    def __symlink_sources_path_to(self, module):
        self.__rm_fr_interface_folder_for(module)

        return self.__lns_s_as_interface_folder_for(module)
