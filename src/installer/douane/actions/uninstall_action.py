import logging


from douane.douane_killer import DouaneKiller
from douane.douane_files_cleaner import DouaneFilesCleaner
from douane.system_cleaner import SystemCleaner
from install_page_step_states import InstallPageStepStates

class UninstallAction:
    """
    This class is responsible to execute the download, compilation
    and installation of Douane, and also to update the progress bar.
    """
    def __init__(self, configuration, on_failure, on_success, on_update):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.configuration = configuration

        self.on_failure = on_failure
        self.on_success = on_success
        self.on_update = on_update

        self.current_install_step = 0

    def run(self):
        # 1. Kill Douane.
        self.current_install_step = 1
        self.on_update(self.current_install_step,
                       InstallPageStepStates.RUNNING)
        suceeded = self.kill_douane()
        self.logger.debug(f'DouaneKiller.kill: {suceeded}')
        if suceeded is False:
            return False
        self.on_update(self.current_install_step,
                       InstallPageStepStates.SUCCEEDED)

        # 2. Remove Douane files.
        self.current_install_step = 2
        self.on_update(self.current_install_step,
                       InstallPageStepStates.RUNNING)
        suceeded = self.remove_douane_files()
        self.logger.debug(f'DouaneFilesCleaner.clean: {suceeded}')
        if suceeded is False:
            return False
        self.on_update(self.current_install_step,
                       InstallPageStepStates.SUCCEEDED)

        # 3. System cleanup
        self.current_install_step = 3
        self.on_update(self.current_install_step,
                       InstallPageStepStates.RUNNING)
        suceeded = self.system_cleanup()
        self.logger.debug(f'SystemCleaner.start: {suceeded}')
        if suceeded is False:
            return False
        self.on_update(self.current_install_step,
                       InstallPageStepStates.SUCCEEDED)

        self.on_success()

    def kill_douane(self):
        douane_killer = DouaneKiller(on_failure=self.on_failure)
        return douane_killer.kill()

    def remove_douane_files(self):
        file_cleaner = DouaneFilesCleaner(configuration=self.configuration,
                                          on_failure=self.on_failure)
        return file_cleaner.clean()

    def system_cleanup(self):
        cleaner = SystemCleaner(configuration=self.configuration,
                                on_failure=self.on_failure)
        return cleaner.start()
