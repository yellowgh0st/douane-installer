import logging


from douane.compiler import DouaneCompiler
from douane.dependencies_installer import DependenciesInstaller
from douane.downloader import DouaneDownloader
from douane.system_cleaner import SystemCleaner
from install_page_step_states import InstallPageStepStates

class InstallAction:
    """
    This class is responsible to execute the download, compilation
    and installation of Douane, and also to update the progress bar.
    """
    def __init__(self, configuration, on_failure, on_autostart_dialog,
                       on_success, on_update):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.configuration = configuration

        self.on_failure = on_failure
        self.autostart_dialog = on_autostart_dialog
        self.on_success = on_success
        self.on_update = on_update

        self.current_install_step = 0

    def run(self):
        # 1. Installs the required packages so that the installer can download,
        #    compile, and install Douane on the system.
        self.current_install_step = 1
        self.on_update(self.current_install_step,
                       InstallPageStepStates.RUNNING)
        suceeded = self.install_dependencies()
        self.logger.debug(f'DependenciesInstaller.install: {suceeded}')
        if suceeded is False:
            return False
        self.on_update(self.current_install_step,
                       InstallPageStepStates.SUCCEEDED)

        # 2. Downloads the Douane source code archives.
        self.current_install_step = 2
        self.on_update(self.current_install_step,
                       InstallPageStepStates.RUNNING)
        suceeded = self.download_douane()
        self.logger.debug(f'DouaneDownloader.start: {suceeded}')
        if suceeded is False:
            return False
        self.on_update(self.current_install_step,
                       InstallPageStepStates.SUCCEEDED)

        # 3. Compile the Douane source code.
        self.current_install_step = 3
        self.on_update(self.current_install_step,
                       InstallPageStepStates.RUNNING)
        suceeded = self.compile_douane()
        self.logger.debug(f'DouaneCompiler.compile: {suceeded}')
        if suceeded is False:
            return False
        self.on_update(self.current_install_step,
                       InstallPageStepStates.SUCCEEDED)

        # 4. Douane startup
        self.current_install_step = 4
        self.on_update(self.current_install_step,
                       InstallPageStepStates.RUNNING)
        self.autostart_dialog()
        if suceeded is False:
            return False
        self.on_update(self.current_install_step,
                       InstallPageStepStates.SUCCEEDED)

        # 5. System cleanup
        self.current_install_step = 5
        self.on_update(self.current_install_step,
                       InstallPageStepStates.RUNNING)
        suceeded = self.system_cleanup()
        self.logger.debug(f'SystemCleaner.start: {suceeded}')
        if suceeded is False:
            return False
        self.on_update(self.current_install_step,
                       InstallPageStepStates.SUCCEEDED)

        self.on_success()

    def install_dependencies(self):
        installer = DependenciesInstaller(configuration=self.configuration,
                                          on_failure=self.on_failure)
        return installer.install()

    def download_douane(self):
        downloader = DouaneDownloader(configuration=self.configuration,
                                      on_failure=self.on_failure)
        return downloader.start()

    def compile_douane(self):
        compiler = DouaneCompiler(configuration=self.configuration,
                                  on_failure=self.on_failure)
        return compiler.compile()

    def system_cleanup(self):
        cleaner = SystemCleaner(configuration=self.configuration,
                                on_failure=self.on_failure)
        return cleaner.start()
