import logging


from douane.process_killer import ProcessKiller
from douane.systemctl import Systemctl

class DouaneKiller():
    def __init__(self, on_failure):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.on_failure = on_failure

    def kill(self):
        self.logger.debug('Killing douane if running ...')

        unit_stopped = self.__stop_douane_units()
        process_killed = self.__kill_douane_dialog_and_daemon()

        return unit_stopped or process_killed

    def __kill_douane_dialog_and_daemon(self):
        """
        In the case something prevented to stop the Douane processes,
        the installer will `kill -9` them all.
        """
        process_killer = ProcessKiller(on_failure=self.on_failure)
        return process_killer.kill(['douaned', 'douane-dialog'])

    def __stop_douane_units(self):
        self.logger.debug('Stopping Douane systemd unit ...')

        systemctl = Systemctl(on_failure=self.on_failure)

        if systemctl.stop('douane-daemon'):
            return True
        else:
            return False
