import logging
import os.path

from douane.command_executor import CommandExecutor

class WgetDownloadManager:
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.destination = None
        self.file_path = None

    def download(self, url, destination):
        self.logger.debug(f'downloading {url} to {destination} ...')

        self.destination = destination

        return self.__build_and_run_download_command(url)

    def on_failure(self, callback):
        self.__on_failure_callback = callback

    def __build_download_command(self, url):
        return ['wget', '-qO', self.destination, url]

    def __build_and_run_download_command(self, url):
        command = self.__build_download_command(url)
        completed_process = CommandExecutor().execute(command)

        if completed_process.returncode != 0:
            self.__on_failure_callback(completed_process.stderr.decode("utf-8"))
            return False
        else:
            return True
