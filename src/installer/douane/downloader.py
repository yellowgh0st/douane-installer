import logging
import os

from douane.decompression_manager_factory import DecompressionManagerFactory
from douane.decompression_managers import TarDecompressionManager
from douane.download_manager_factory import DownloadManagerFactory
from douane.download_managers.wget import WgetDownloadManager

class DouaneDownloader():
    def __init__(self, on_failure, configuration):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.on_failure = on_failure
        self.configuration = configuration

    def start(self):
        self.logger.debug('start ...')
        decompression_manager = self.__initialise_decompression_manager()
        decompression_manager.on_failure(self.on_failure)

        download_manager = self.__initialise_download_manager()
        download_manager.on_failure(self.on_failure)

        for index, repository in enumerate(
            self.configuration.repositories,
            start=1
        ):
            self.logger.debug(f'Downloading {repository["name"]} from URL '
                              f'{repository["url"]} ...')
            url_with_extension = ".".join([
                repository["url"],
                self.configuration.douane_archive_extension
            ])

            if not download_manager.download(
                url_with_extension,
                self.__download_destination_for(repository)
            ):
                return False

            if not decompression_manager.decompress(
                self.__download_destination_for(repository)
            ):
                return False

            self.__update_configuration_repository(
                index=(index - 1),
                final_url=url_with_extension,
                sources_path=decompression_manager.folder_path
            )
            self.logger.debug(f'{self.configuration.repositories}')

        return True

    def __download_destination_for(self, repository):
        return os.path.join(
            self.configuration.working_directory,
            ".".join([repository["source_folder_name"],
                      self.configuration.douane_archive_extension])
        )

    def __initialise_decompression_manager(self):
        factory = DecompressionManagerFactory()

        return factory.create(self.configuration.decompression_manager)

    def __initialise_download_manager(self):
        factory = DownloadManagerFactory()

        return factory.create(self.configuration.download_manager)

    def __update_configuration_repository(self, index, final_url, sources_path):
        self.logger.debug(f'Updating repository at position {index} with '
                          f'URL {final_url} and '
                          f'sources_path: {sources_path} ...')
        self.configuration.repositories[index]["url"] = final_url
        self.configuration.repositories[index]["sources_path"] = sources_path
