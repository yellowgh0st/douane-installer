import logging
import sys
sys.path.append('..')

from douane.command_executor import CommandExecutor

from i18n import _

class ProcessKiller():
    def __init__(self, on_failure):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.on_failure = on_failure

    def kill(self, processes_name):
        self.logger.debug(f'Trying to kill process(es) {processes_name} ...')

        if isinstance(processes_name, list):
            killed = True

            for process_name in processes_name:
                if not self.__try_to_kill(process_name):
                    killed = False

            return killed
        else:
            return self.__try_to_kill(processes_name)

    def __try_to_kill(self, process_name):
        if self.__process_is_running(process_name):
            command = self.__build_kill_command(process_name)
            completed_process = CommandExecutor().execute(command)

            if completed_process.returncode != 0:
                self.logger.debug(f'ERROR: kill -9 {self.process_pid} failed: '
                                  f'{completed_process.stderr.decode("utf-8")}')
                self.on_failure(completed_process.stderr.decode("utf-8"))

            return completed_process.returncode == 0
        else:
            return False

    def __build_kill_command(self, process_name):
        return ["kill", "-9", self.process_pid]

    def __fetch_process_path_from(self, process_name):
        which_process = CommandExecutor().execute(["which", process_name])

        if which_process.returncode != 0:
            self.logger.debug(f'ERROR: which {process_name} failed: '
                              f'{which_process.stderr.decode("utf-8")}')
            if len(which_process.stderr) == 0:
                self.on_failure(_('{0} cannot be found.').format(process_name))
            else:
                self.on_failure(which_process.stderr.decode("utf-8"))
            return False

        process_path = which_process.stdout.decode("utf-8").rstrip()

        # In the case there is a symlink, look for the real process path
        # (Actually the douaned and douane-dialog are symlinks ;-))
        readlink_process = CommandExecutor().execute(["readlink", process_path])

        if readlink_process.returncode != 0:
            self.logger.debug(f'ERROR: readlink {process_path} failed: '
                              f'{readlink_process.stderr.decode("utf-8")}')
            self.on_failure(readlink_process.stderr.decode("utf-8"))
            return False

        return readlink_process.stdout.decode("utf-8").rstrip()

    def __process_is_running(self, process_name):
        command_path = self.__fetch_process_path_from(process_name)

        if not command_path:
            return False

        self.logger.debug(f'{process_name} is located at {command_path}')

        completed_process = CommandExecutor().execute(["ps", "ax"])

        if completed_process.returncode != 0:
            self.on_failure(completed_process.stderr.decode("utf-8"))

        self.process_pid = None

        for line in completed_process.stdout.splitlines():
            decoded_line = line.decode("utf-8")

            if command_path in decoded_line:
                self.logger.debug(f'{command_path} found in {decoded_line}!')
                # First part is the PID
                self.process_pid = decoded_line.split()[0]

        self.logger.debug(f'{command_path} found?: {self.process_pid}')

        return self.process_pid != None
