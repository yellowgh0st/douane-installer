import logging


from douane.command_executor import CommandExecutor

class PythonInstaller():
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

    def name(self):
        return 'Python Installer'

    def compile(self, sources_path):
        self.logger.debug(f'Installing python app from {sources_path} ...')

        command = self.__build_dkms_command()
        completed_process = CommandExecutor().execute(command, sources_path)

        return completed_process.returncode == 0

    def uninstall(self, sources_path):
        return ["python3", "uninstall.py"]

    def __build_dkms_command(self):
        return ["python3", "setup.py", "install"]
