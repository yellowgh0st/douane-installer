import logging
import os
import tempfile


class DouaneInstallerPipeBuilder:
    """
    Updates the configuration's douane_pipe_path and creates the pipe file.
    """
    def __init__(self, configuration):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.configuration = configuration

    def build(self):
        self.logger.debug('Building named pipe ...')

        self.configuration.douane_pipe_path = self.__pipe_path()

        self.logger.debug(
            f'Opening pipe at {self.configuration.douane_pipe_path} ...'
        )
        os.mkfifo(self.configuration.douane_pipe_path)

    def destroy(self):
        self.logger.debug('Destroying named pipe ...')

        try:
            os.remove(self.configuration.douane_pipe_path)
            os.rmdir(os.path.dirname(self.configuration.douane_pipe_path))
        except FileNotFoundError:
            pass

    def __pipe_path(self):
        return os.path.join(tempfile.mkdtemp(), 'douane_installer')
