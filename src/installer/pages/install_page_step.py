import logging

from gi.repository import Gtk


from ..install_page_step_states import InstallPageStepStates


class InstallPageStep:
    ICON_NEW = 'emblem-generic'
    ICON_SUCCESS = 'emblem-default'
    ICON_FAILURE = 'emblem-important'

    ICON_SPACING = 15

    SPINNER_SIZE = 25

    def __init__(self, label):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.label = label
        self.state = InstallPageStepStates.NEW

    def build_step(self):
        step_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        content = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

        icon_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.icon = self.__build_step_icon(InstallPageStep.ICON_NEW)
        icon_box.pack_start(self.icon, True, True, InstallPageStep.ICON_SPACING)

        content.pack_start(icon_box, False, False, 0)

        text = Gtk.Label(self.label)
        content.pack_start(text, False, False, 0)

        step_box.pack_start(content, True, True, 5)

        return step_box

    def update(self, state):
        self.logger.debug(f'[{self.label}] update(state:{state}) ...')
        if state == InstallPageStepStates.RUNNING:
            self.__update_state(InstallPageStepStates.RUNNING)
        elif state == InstallPageStepStates.SUCCEEDED:
            self.__update_state(InstallPageStepStates.SUCCEEDED)
        elif state == InstallPageStepStates.FAILED:
            self.__update_state(InstallPageStepStates.FAILED)
        else:
            raise ValueError(state)

    def __build_step_icon(self, name):
        self.logger.debug(f'[{self.label}] __build_step_icon(name:{name}) ...')
        return Gtk.Image.new_from_icon_name(name, Gtk.IconSize.DND)

    def __build_step_spinner(self):
        self.logger.debug(f'[{self.label}] __build_step_spinner()')
        spinner = Gtk.Spinner()
        spinner.start()
        spinner.set_size_request(InstallPageStep.SPINNER_SIZE,
                                 InstallPageStep.SPINNER_SIZE)
        return spinner

    def __replace_icon_with(self, widget):
        parent = self.icon.get_parent()
        parent.remove(self.icon)
        self.icon = widget

        parent.pack_start(widget, True, True, self.__box_spacing_for(widget))
        self.icon.show()

    def __update_state(self, state):
        self.state = state

        self.__update_step_icon()

    def __update_step_icon(self):
        if self.state == InstallPageStepStates.RUNNING:
            self.__replace_icon_with(self.__build_step_spinner())
        elif self.state == InstallPageStepStates.SUCCEEDED:
            self.__replace_icon_with(
                self.__build_step_icon(InstallPageStep.ICON_SUCCESS)
            )
        elif self.state == InstallPageStepStates.FAILED:
            self.__replace_icon_with(
                self.__build_step_icon(InstallPageStep.ICON_FAILURE)
            )

    def __box_spacing_for(self, widget):
        if widget.__class__.__name__ == 'Spinner':
            return InstallPageStep.ICON_SPACING + 3
        else:
            return InstallPageStep.ICON_SPACING
