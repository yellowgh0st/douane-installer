from gi.repository import Gtk

from installer.douane.command_executor import CommandExecutor
from installer.pages.page import Page

from installer.i18n import _


class SummaryPage(Page):
    def __init__(self, assistant):
        super().__init__(assistant)

        self.page = None
        self.page_title = _('Summary')
        self.page_type = Gtk.AssistantPageType.SUMMARY

    def page_content(self, page):
        # Top box shows a sentence telling the user if Douane has been installed
        # successfully or if it has failed.
        top_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.summary_label = Gtk.Label('Summary')
        self.summary_label.set_line_wrap(True)
        top_box.pack_end(self.summary_label, False, False, 0)
        page.pack_start(top_box, True, True, 0)

        # The bottom box shows the failure reason when installation failed,
        # otherwise it shows a short description of the next step to the user
        # and shows an "Open Configurator" button so that the user can open it
        # and see the available options (the user is not lost after the install)
        bottom_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        # Error label
        # A text is required, otherwise you'll get strange crashes
        self.error_label = Gtk.Label('Error')
        self.error_label.set_line_wrap(True)
        self.error_label.set_margin_top(50)
        bottom_box.pack_start(self.error_label, False, False, 0)

        # Next step description and open configurator button
        self.next_step_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        explanation = Gtk.Label(
            _('Now that Douane is installed you will see quite shorty a'
              "dialog appearing when an app, that Douane doesn't know, tries"
              'to access the Internet where you will be able to accept,'
              'refuse or ignore.\n\n'
              'The bellow button allows you to open the Douane configurator'
              'which allows you to stop/start Douane and manage rules.')
        )
        explanation.set_line_wrap(True)
        self.next_step_box.pack_start(explanation, True, True, 0)

        open_configurator_button = Gtk.Button.new_with_label(
            _('Open Douane Configurator')
        )
        open_configurator_button.connect('clicked',
                                         self.__on_open_configurator_clicked)
        self.next_step_box.pack_start(open_configurator_button, False, False, 0)
        open_configurator_button.show()

        self.next_step_box.set_margin_bottom(50)

        bottom_box.pack_start(self.next_step_box, True, True, 0)
        page.pack_start(bottom_box, True, True, 0)

        return page

    def __on_open_configurator_clicked(self, button):
        CommandExecutor().spwan('douane-configurator')
