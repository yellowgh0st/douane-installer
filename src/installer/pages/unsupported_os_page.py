from gi.repository import Gtk

from installer.image_builder import ImageBuilder
from installer.pages.page import Page

from installer.i18n import _


class UnsupportedOsPage(Page):
    def __init__(self, assistant):
        super().__init__(assistant)

        self.page_title = _('Summary')
        self.page_type = Gtk.AssistantPageType.SUMMARY

    def page_content(self, page):
        # Douane logo
        logo = ImageBuilder('media/douane_logo.svg', 256).build()
        page.pack_start(logo, False, False, 0)

        # Douane title
        title = Gtk.Label()
        title.set_markup('<b><big>{0}</big></b>'.format(
            _('Douane personal firewall for GNU/Linux')
        ))
        page.pack_start(title, False, False, 0)
        sub_title = Gtk.Label()
        sub_title.set_markup(_(
            "Douane is a personal firewall that protects a user's privacy by "
            "allowing a user to control which applications can connect to "
            "the internet from their GNU/Linux computer."))
        sub_title.set_line_wrap(True)
        page.pack_start(sub_title, True, True, 0)

        description = Gtk.Label()
        description.set_markup('<big>{0}</big>'.format(
            _(
                "The Douane firewall project is only available on Linux, "
                "therefore the installer can't continue."
            )
        ))
        page.pack_start(description, True, True, 0)

        return page

