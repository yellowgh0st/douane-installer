import logging

from gi.repository import GLib, Gtk

from installer.douane_installer_listener import DouaneInstallerListener
from installer.douane_installer_pipe_builder import DouaneInstallerPipeBuilder
from installer.douane_installer_runner import DouaneInstallerRunner
from installer.image_builder import ImageBuilder
from installer.pages.install_page_step import InstallPageStep
from installer.pages.page import Page

from installer.i18n import _


class InstallPage(Page):
    def __init__(self, assistant, configuration):
        super().__init__(assistant)

        self.logger = logging.getLogger(self.__class__.__name__)

        self.assistant = assistant
        self.configuration = configuration
        self.pipe_builder = None
        self.action_is_running = False

        self.page_title = _('Installing Douane')
        self.page_type = Gtk.AssistantPageType.PROGRESS

        self.steps = []

    def page_content(self, page):
        top = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
        # Douane logo
        logo = ImageBuilder('media/douane_logo.svg', 48).build()
        top.pack_start(logo, False, False, 0)

        description = Gtk.Label()
        description.set_markup(
            '<big>{0}</big>\n<small>{1}</small>'.format(_(
                'The assistant is now downloading, compiling and '
                'installing douane ...'), _(
                'You can see the installation progress with the bellow steps.'
            ))
        )
        top.pack_start(description, False, False, 0)

        page.pack_start(top, False, False, 0)

        # Steps box
        steps_aligner = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        steps_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.__build_steps_in(steps_box)
        steps_aligner.pack_start(Gtk.Box(), True, True, 0)
        steps_aligner.pack_start(steps_box, True, False, 0)
        steps_aligner.pack_start(Gtk.Box(), True, True, 0)
        page.pack_start(steps_aligner, True, False, 0)

        return page

    def on_page_prepared(self):
        """
        When the user click the next button and is now on the InstallPage
        """
        self.logger.debug('[InstallPage] on_page_prepared!')

        self.pipe_builder = DouaneInstallerPipeBuilder(
                              configuration=self.configuration
                            )
        self.pipe_builder.build()

        # Create the installer listener in a thread
        DouaneInstallerListener(configuration=self.configuration,
                                on_failure=self.__on_install_failure,
                                on_success=self.__on_install_success,
                                on_step_update=self.__on_step_update).start()

        # Run the install process
        DouaneInstallerRunner(configuration=self.configuration,
                              on_failure=self.__on_install_failure,
                              on_finished=self.__on_worker_finished).run()

    def __build_steps_in(self, box):
        step1 = InstallPageStep(_('Compilation dependencies installation'))
        self.steps.append(step1)
        box.pack_start(step1.build_step(), False, True, 0)

        step2 = InstallPageStep(_('Douane source code download'))
        self.steps.append(step2)
        box.pack_start(step2.build_step(), False, True, 0)

        step3 = InstallPageStep(
            _('Douane source code compilation and installation')
        )
        self.steps.append(step3)
        box.pack_start(step3.build_step(), False, True, 0)

        step4 = InstallPageStep(_('Douane startup'))
        self.steps.append(step4)
        box.pack_start(step4.build_step(), False, True, 0)

        step5 = InstallPageStep(_('System cleanup'))
        self.steps.append(step5)
        box.pack_start(step5.build_step(), False, True, 0)

    def __on_install_failure(self, error=None):
        """
        In the case the installer fails to download, compile or install Douane,
        this method is fired.
        """
        error = error.rstrip()

        self.action_is_running = False

        self.pipe_builder.destroy()

        GLib.idle_add(self.assistant.summary_page.summary_label.set_text,
                      _('Douane installation has failed!'))

        error_message = _('<b>Reason:</b> {0}').format(error)
        self.logger.debug(f'error_message: {error_message}')
        GLib.idle_add(self.assistant.summary_page.error_label.set_markup,
                      error_message)

        GLib.idle_add(self.assistant.summary_page.next_step_box.hide)

        self.logger.debug('Switching to the Summary Assistant page ...')
        self.assistant.set_page_complete(self, True)
        self.assistant.set_current_page(self.assistant.get_current_page() + 1)

    def __on_install_success(self):
        """
        Installation finished on success.
        """
        self.logger.debug('__on_install_success.')

        self.action_is_running = False

        self.pipe_builder.destroy()

        GLib.idle_add(self.assistant.summary_page.summary_label.set_text,
                      _('Douane has been successfully installed!'))

        GLib.idle_add(self.assistant.summary_page.error_label.hide)

        self.assistant.set_page_complete(self, True)
        self.assistant.set_current_page(self.assistant.get_current_page() + 1)

    def __on_step_update(self, index, state):
        self.action_is_running = True

        self.logger.debug(f'__on_step_update(index:{index}, '
                          f'state: {state}) ...')
        self.steps[index - 1].update(state)

    def __on_worker_finished(self):
        self.logger.debug('Worker has finished')

        if self.action_is_running:
            """
            The worker ended while the installation is ongoing, so something
            went wrong.

            Therefore the installer should move on to the summary page and tell
            that something was wrong.
            """
            self.logger.debug('Oops, installer was not finished.')

            self.__on_install_failure(
                _('Worker ended while the installation is still running')
            )
