from gi.repository import Gtk

from installer import VERSION
from installer.image_builder import ImageBuilder
from installer.pages.page import Page

from installer.i18n import _


class WelcomePage(Page):
    def __init__(self, assistant, uninstall=False):
        super().__init__(assistant)

        self.page_title = _('Welcome !')
        self.page_type = Gtk.AssistantPageType.INTRO
        self.uninstall = uninstall

    def page_content(self, page):
        # Douane logo
        logo = ImageBuilder('media/douane_logo.svg', 256).build()
        page.pack_start(logo, False, False, 0)

        # Douane title
        title = Gtk.Label()
        title.set_markup(
            '<b><big>{0}</big></b>\n<small>Version {1}</small>'.format(
                _('Douane personal firewall for GNU/Linux'),
                VERSION
            )
        )
        title.set_justify(Gtk.Justification.CENTER)
        page.pack_start(title, False, False, 0)
        sub_title = Gtk.Label()
        sub_title.set_markup(_(
            "Douane is a personal firewall that protects a user's privacy by "
            "allowing a user to control which applications can connect to "
            "the internet from their GNU/Linux computer."))
        sub_title.set_line_wrap(True)
        page.pack_start(sub_title, True, True, 0)

        description = Gtk.Label()
        description.set_markup('{0}\n{1}'.format(
            self.__assistant_goal_description(),
            _(
                'Click <b>{0}</b> in order to proceed or <b>{1}</b> to abort.'
            ).format(_('Next'), _('Cancel'))
        ))
        page.pack_start(description, True, True, 0)

        return page

    def __assistant_goal_description(self):
        if self.uninstall:
            return _(
                'This assistant will help you to uninstall the Douane firewall.'
            )
        else:
            return _(
                'This assistant will help you to download, compile and install'
                ' the Douane firewall.'
            )
