class Configuration:
    """
    The Configuration class stores the various configuration keys which are
    used by the installer in order to configure it in various place in the code.
    """
    def __init__(self, args):
        # Is the installer running with the debug flag enabled
        self.debug = args.debug

        # Should the installer uninstall Douane?
        if hasattr(args, 'action'):
            self.uninstall = args.action == 'uninstall'
        elif hasattr(args, 'uninstall'):
            self.uninstall = args.uninstall
        else:
            self.uninstall = False

        # The path to the pipe file used by the douane-installer in order to
        # run the installer as root (asking the root password only once), and
        # update the UI.
        # See DouaneInstallerPipeBuilder, DouaneInstallerListener and
        # DouaneInstallerEventBroadcaster.
        if hasattr(args, 'pipe'):
            self.douane_pipe_path = args.pipe
        else:
            self.douane_pipe_path = None

        # Douane tag to be downloaded
        self.tag = args.tag

        self.os = None

        self.working_directory = '/usr/src/'

        # ~~~~~~~~~~~~~~~~~~~~
        # Douane respositories
        #
        # Repositories stores the different pieces of Douane information like
        # its source code download URL (without the extension as the Environment
        # class will determine which unarchiver is available, and then the
        # installer will update the URL with the right extension) and more.
        # ~~~~~~~~~~~~~~~~~~~~
        self.repositories = [
            {
                'name': 'dkms',
                'url': f'https://gitlab.com/douaneapp/douane-dkms/-/archive/v{self.tag}/douane-dkms-v{self.tag}',
                'source_folder_name': 'douane-dkms',
                'sources_path': None,
                'compilation_type': 'dkms'
            },
            {
                'name': 'dbus',
                'url': f'https://gitlab.com/douaneapp/douane-dbus/-/archive/v{self.tag}/douane-dbus-v{self.tag}',
                'source_folder_name': 'douane-dbus',
                'sources_path': None,
                'compilation_type': 'symlink',
                'compilation_options': {
                    'link_in_to': [
                        'daemon',
                        'dialog'
                    ]
                }
            },
            {
                'name': 'daemon',
                'url': f'https://gitlab.com/douaneapp/douane-daemon/-/archive/v{self.tag}/douane-daemon-v{self.tag}',
                'source_folder_name': 'douane-daemon',
                'sources_path': None,
                'compilation_type': 'make'
            },
            {
                'name': 'dialog',
                'url': f'https://gitlab.com/douaneapp/douane-dialog/-/archive/v{self.tag}/douane-dialog-v{self.tag}',
                'source_folder_name': 'douane-dialog',
                'sources_path': None,
                'compilation_type': 'make'
            },
            {
                'name': 'configurator',
                'url': f'https://gitlab.com/douaneapp/douane-configurator/-/archive/v{self.tag}/douane-configurator-v{self.tag}',
                'source_folder_name': 'douane-configurator',
                'sources_path': None,
                'compilation_type': 'python'
            }
        ]

        # ~~~~~~~~~~~~~~~~~~
        # Linux
        # ~~~~~~~~~~~~~~~~~~

        # Linux distribution name, version and code name as a Tuple
        # like ('elementary', '5.1', 'hera')
        self.distribution = None

        # Is the distribution a Debian based one
        self.debian_based = False

        # Is the distribution a Fedora based one
        self.fedora_based = False

        # tar, unzip, ...
        self.decompression_manager = None

        # tar.gz, zip, ...
        self.douane_archive_extension = None

        # wget, curl, ...
        self.download_manager = None

        # APT, RPM, ...
        self.package_format = None
