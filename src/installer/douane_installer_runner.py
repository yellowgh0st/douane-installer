import logging
import os
import threading


from gi.repository import Gtk

from installer.douane.command_executor import CommandExecutor

class DouaneInstallerRunner:
    """
    Runs the DouaneInstaller as root via pkexec so that the root password is
    asked only once.
    """
    def __init__(self, configuration, on_failure, on_finished):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.configuration = configuration
        self.on_failure = on_failure
        self.on_finished = on_finished

    def run(self):
        self.logger.debug('Creating thread running the worker process ...')
        threading.Thread(target=self.__run_thread, daemon=True).start()

    def __run_thread(self):
        self.logger.debug('Starting the thread running the worker process ...')

        command = self.__build_install_command()
        completed_process = CommandExecutor().execute(command)

        self.logger.debug('worker process ended with exit code '
                          f'{completed_process.returncode} ...')

        if completed_process.returncode != 0:
            self.on_failure(completed_process.stderr.decode("utf-8"))

        self.on_finished()

        self.logger.debug('DouaneInstallerRunner LEAVED !')

    def __build_install_command(self):
        dirname = os.path.dirname(__file__)
        self.logger.debug(f'Current path: {dirname}')
        installer_path = os.path.join(dirname, 'worker')
        self.logger.debug(f'installer_path: {installer_path}')

        command = ["pkexec", installer_path]
        args = [
            "--pipe", self.configuration.douane_pipe_path,
            "--tag", self.configuration.tag
        ]

        if self.configuration.debug:
            args.append("--debug")

        if self.configuration.uninstall:
            args.append("--action")
            args.append("uninstall")

        return command + args
