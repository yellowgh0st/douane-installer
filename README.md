# Douane Installer

![](media/douane-installer.png)

Douane installer is a Graphical User Interface application which helps in compiling and installing Douane on a Linux system.

It follows exactely the description [installation procedure](https://gitlab.com/douaneapp/Douane/-/wikis/Compilation) from the Wiki.

It covers the lack of packagers for the project, but as soon as a system package is available, it is recommended to use it instead of douane installer.

If you'd like to chat about Douane, join us on [![Gitter](https://badges.gitter.im/Douane/General.svg)](https://gitter.im/Douane/General?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)!

# Usage

In the case you're facing an issue, you can enable debug mode by passing the `--debug` or `-d` flag to the installer.

## Requirements

This installer has very little requirements (on purpose), here is the required packages required.

### Debian and Debian based distributions

* policykit-1
* python-minimal
* tar
* wget

You can install them with:

```
apt update && apt install policykit-1 python-minimal tar wget
```

## Download the installer

To run the installer on your system, please follow those instructions.

In a terminal:

```bash
wget -O douane-installer.tar.gz \
        https://gitlab.com/douaneapp/douane-installer/-/archive/v0.8.2/douane-installer-v0.8.2.tar.gz
mkdir douane-installer \
    && tar xzf douane-installer.tar.gz \
           --directory=douane-installer \
           --strip-components 1
rm -f douane-installer.tar.gz
cd douane-installer/
```

## Install Douane

You can run the installer with 2 ways.

### From the terminal

```
./douane-installer
```

### From the user interface

1. Open your file browser from the `douane-installer` folder:
  ```
  xdg-open .
  ```
2. Double click the `Douane Installer`.
3. You will be asked to `Trust and Launch` the installer:
  ![](media/asked-to-trust-desktop-file.png)

### After the installation

You should see the following window appearing quite quickly in the case you have running applications using the Internet:

![](media/douane-dialog.png)

## Uninstall Douane

First, that's sad to see you leaving this project.

To uninstall Douane:

```
cd douane-installer/
./douane-installer --uninstall
```

# Supported Linux distributions

* Debian
* Elementary OS

## Adding support for Linux distributions

![](media/douane-installer_unsupported-dist.png)

First of all, thank you very much for your interest in this project!

Please find all the information you need im order to add the support of a new
Linux distribution in [this wiki page](https://gitlab.com/douaneapp/douane-installer/-/wikis/Add-support-for-a-new-Linux-distribution).

## Translation

### Supported translations

* English (Base language)
* French

If you want to add new one, please open a new MR.

Please find how to add and update translations in [this wiki page](https://gitlab.com/douaneapp/douane-installer/-/wikis/Manage-translations).
